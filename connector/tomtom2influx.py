from influxdb import InfluxDBClient


# Create DB, specifying host, port and db name
def createDB(host='localhost', port=8086, dbname=None):
    client = InfluxDBClient(host=host, port=port, database=dbname)
    client.create_database(dbname=dbname)
    return client

# Write data into influx. Note that data must be a json file with format like this:
"""json_body = [
            {
                "measurement": "street_data",
                "fields": {
                    'Hour': now.hour,
                    'Day': now.day,
                    'Month': now.month,
                    'DayOfTheWeek': weekno,
                    'currentSpeed': float(root['currentSpeed']),
                    'freeFlowSpeed': float(root['freeFlowSpeed']),
                    'currentTravelTime': int(root['currentTravelTime']),
                    'freeFlowTravelTime': int(root['freeFlowTravelTime']),
                    'streetName': sector['name'],
                    'latitudePoint1': coordinates[0][1],
                    'longitudePoint1': coordinates[0][0],
                    'latitudePoint2': coordinates[1][1],
                    'longitudePoint2': coordinates[1][0]
                    #'geometry': coordinates,
                }
            },
        ]
"""

def write2Influx(influxClient, database, measurement):
    json_body = measurement
    influxClient.write_points(json_body, database=database, protocol='json')
