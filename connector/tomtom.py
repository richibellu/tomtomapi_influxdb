# -*- coding: utf-8 -*-

import json

import requests

import datetime

class TomTomApi:
    def __init__(self, configuration):
        self.config = configuration
        self.session = requests.Session()
        self.rq_config = configuration['rest']
        self.params = self.rq_config['params']

    def coords_to_geojson(self, coords):
        return list([float(point[0]), float(point[1])] for point in coords)

    def calc_extra_map_info(self, elem):
        current = elem['currentSpeed']
        free_flow = elem['freeFlowSpeed']
        percentage = current * 100 / free_flow
        if percentage < 50:
            return 2
        elif percentage < 80:
            return 1
        else:
            return 0

    def get_street_data(self, sector):        
        coords = sector['coords']
        if not coords or len(coords[0]) < 2:
            return {}
        self.params['point'] = "{},{}".format(
            coords[0][1], coords[0][0])
        try:
            r = self.session.get(
                self.rq_config['url'],
                params=self.params,
                headers=self.rq_config['headers'],
                auth=(self.rq_config["user"], self.rq_config["password"])
            )
            if r.status_code != 200 and r.status_code != 409:
                r.raise_for_status()
        except (requests.exceptions.RequestException, ValueError) as e1:
            return {}

        root = json.loads(r.content)
        # Get root element
        root = self.resolve(root, 'flowSegmentData')
        now = datetime.datetime.now()
        weekno = datetime.datetime.today().weekday()

        coordinates = self.coords_to_geojson(coords)

        json_body = [
            {
                "measurement": "street_data",
                "fields": {
                    'Hour': now.hour,
                    'Day': now.day,
                    'Month': now.month,
                    'DayOfTheWeek': weekno,
                    'currentSpeed': float(root['currentSpeed']),
                    'freeFlowSpeed': float(root['freeFlowSpeed']),
                    'currentTravelTime': int(root['currentTravelTime']),
                    'freeFlowTravelTime': int(root['freeFlowTravelTime']),
                    'streetName': sector['name'],
                    'latitudePoint1': coordinates[0][1],
                    'longitudePoint1': coordinates[0][0],
                    'latitudePoint2': coordinates[1][1],
                    'longitudePoint2': coordinates[1][0]
                    #'geometry': coordinates,
                }
            },
        ]

        # data = {
        #     'Hour': now.hour,
        #     'Day' : now.day,
        #     'Month' : now.month,
        #     'DayOfTheWeek': weekno,
        #     'currentSpeed': float(root['currentSpeed']),
        #     'freeFlowSpeed': float(root['freeFlowSpeed']),
        #     'currentTravelTime': int(root['currentTravelTime']),
        #     'freeFlowTravelTime': int(root['freeFlowTravelTime']),
        #     #'geometry': self.coords_to_geojson(coords),
        #     'streetName': sector['name'],
        #     #'extra_map_info': self.calc_extra_map_info(root)
        # }

        return json_body



    # Get object attribute from a string where keys are separated by dot

    def resolve(self, obj, attrspec):
        if attrspec:
            for attr in attrspec.split("."):
                try:
                    if attr.isdigit():
                        obj = obj[int(attr)]
                    else:
                        obj = obj[attr]
                except TypeError:
                    obj = getattr(obj, attr)
                except KeyError:
                    raise
        return obj
