# -*- coding: utf-8 -*-
import time
import yaml
import json

import tomtom_api_for_nn.connector.tomtom as tomtom
import tomtom_api_for_nn.connector.tomtom2influx as tt2influx

# Create influxdb
dbname = 'tomtom_data'
client = tt2influx.createDB(dbname=dbname)

def read_config(filename):
    try:
        with open(filename, 'r') as f:
            config = yaml.load(f)
            return config
    except Exception as e:
        print('Configuration Error')
        raise e

def read_streets(filename):
        streets = []
        with open(filename, 'r') as data:
            for line in data:
                street_data = line.split(";")
                coords = street_data[2].strip().split(" ")
                coords = [point.split(",") for point in coords]
                street = {"id": street_data[0], "name": street_data[1], "coords": coords}
                streets.append(street)
        return streets[1:]


# Main loop of the connector
def connector_loop(config, tomtomapi):
    while True:
        start_time = time.time()
        # Get data from external Sources
        streets = config["streets"]
        ids = []

        for street in streets:
            data = tomtomapi.get_street_data(street)
            key = str((street['id']))
            ids.append(key)

            # Write into InfluxDB
            tt2influx.write2Influx(client, dbname, data)

        # Elasped time for sleep interval
        elapsed_time = time.time() - start_time

        # Wait until is time to launch again
        sleep_interval = config["interval"] - elapsed_time
        if sleep_interval > 0:
            # Only sleep what's necessary so the interval is applied
            # between the beginning of each run
            time.sleep(sleep_interval)
        elif config["interval"] == 0:
            # For testing we can run only once doing: export CON_CONNECTOR_INTERVAL=0
            break

def main():
    config_obj = read_config("assets/config/tom_tom.yml")
    streets = read_streets(config_obj["data_file"])
    config_obj["streets"] = streets

    # Create tomtom class instance
    tomtom_instance = tomtom.TomTomApi(config_obj)

    # Call main connector loop
    connector_loop(config_obj,tomtom_instance)

if __name__ == '__main__':
    main()
