# -*- coding: utf-8 -*-
import logging
import os
import re
import sys
import time
import yaml

import tomtom
import cosapi

from cosapi import Mobility
from slugify import slugify




log = logging.getLogger(__name__)

"""
Add support to environment variables to yaml
Example: 
    baseUrl: <%= ENV['CON_CONNECTOR_BASE_URL'] %>
"""

pattern = re.compile(r'^\<%= ENV\[\'(.*)\'\] %\>(.*)$')
yaml.add_implicit_resolver("!env", pattern)


def varaiblenv_constructor(loader, node):
    value = loader.construct_scalar(node)
    env_var, remaining_path = pattern.match(value).groups()
    if env_var in os.environ:
        data = yaml.load(os.environ[env_var])
        return data


yaml.add_constructor('!env', varaiblenv_constructor)


# Create an object in mobility to hold data
def create_objects(mobility, config):
    log.info("Creating all objects")

    obj = {
        'object_type': config['object']['type'],
        'name': config['object']['name'],
        'core': False
    }

    mobility.create_object(obj)

# Create all schemas
def create_schemas(config, mobility):
    log.info("Creating all schemas")
    mobility.create_schema(config['object']['type'], config['schema'])


# Main loop of the connector
def connector_loop(config, tomtomapi, cosapi): #
    while True:
        start_time = time.time()
        # Get data from external Sources
        streets = config["streets"]
        ids = []
        for street in streets:
            data = tomtomapi.get_street_data(street)
            key = str(slugify(unicode(street['id'])))
            cosapi.send_data_to_mobility(key, data)
            ids.append(key)
        cosapi.delete_others(ids)
        # Log elapsed time
        elapsed_time = time.time() - start_time
        log.info("Connector process time: %s", elapsed_time)

        # Wait until is time to launch again
        sleep_interval = config["interval"] - elapsed_time
        if sleep_interval > 0:
            # Only sleep what's necessary so the interval is applied
            # between the beginning of each run
            time.sleep(sleep_interval)
        elif config["interval"] == 0:
            # For testing we can run only once doing: export CON_CONNECTOR_INTERVAL=0
            break


def init_log(level):
    log_format = "%(asctime)s %(name)-12s %(funcName)20s:%(lineno)s %(levelname)-8s %(message)s"
    logging.basicConfig(stream=sys.stdout, level=level, format=log_format)
    logging.getLogger("requests").setLevel(level)


def read_config(filename):
    log.info("Reading config file {}".format(filename))
    try:
        with open(filename, 'r') as f:
            config = yaml.load(f)
            return config
    except Exception as e:
        log.exception(e)
        raise e

# Get external id key name from schema
def get_extenal_id(schema):
    for key in schema['properties']:
        if '$ref' in schema['properties'][key] and schema['properties'][key]['$ref'] == '#/external_id':
            return key

    raise Exception('No external_id found in the schema')

def read_streets(filename):
    streets = []
    with open(filename, 'r') as data:
        for line in data:
            street_data = line.split(";")
            coords = street_data[2].strip().split(" ")
            coords = [point.split(",") for point in coords]
            street = {"id": street_data[0], "name": street_data[1], "coords": coords}
            streets.append(street)
    return streets[1:]


def main():
    init_log(logging.INFO)
    config_obj = read_config("assets/config/tom_tom.yml")
    log.info(config_obj)
    streets = read_streets(config_obj["data_file"]) #
    config_obj["streets"] = streets #
    # Create mobility class
    mobility_instance = Mobility(
        log, config_obj, get_extenal_id(config_obj['schema']))

    # Create objects
    create_objects(mobility_instance, config_obj)

    # If configured to do so, create schemas
    if config_obj['createSchema']:
        create_schemas(config_obj, mobility_instance)

    # Set cross layer (if is defined)
    if config_obj['crossLayer']:
        if os.path.isfile("assets/map/tom_tom.css"):
            with open("assets/map/tom_tom.css", 'r') as data_file:
                config_obj['crossLayer']['cartocss'] = unicode(
                    data_file.read().replace('\n', '').replace('\r', ''))
                data_file.close()
        mobility_instance.set_cross_layer(
            config_obj['object']['type'], config_obj['crossLayer'])
    # Set popup (if is defined)
    if os.path.isfile("assets/popup/tom_tom.ejs"):
        with open("assets/popup/tom_tom.ejs", 'r') as data_file:
            html = data_file.read().replace('\n', '').replace('\r', '')
            data_file.close()
        popup = {'html': html}
        mobility_instance.set_popup_layer(config_obj['object']['type'], popup)

    # Create tomtom class instance
    tomtom_instance = tomtom.TomTomApi(log, config_obj)
    # Call main connector loop
    connector_loop(config_obj, tomtom_instance, mobility_instance)

if __name__ == "__main__":
    main()
