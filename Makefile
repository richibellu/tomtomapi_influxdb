DOCKER_NAME=worldsensing/mbconnector_tomtom_bcn_cos
VERSION=1.0.1
CONNECTOR_CONTAINER_NAME=mbconnector_tomtom_bcn_cos
DOCKER_NAME_FULL=$(DOCKER_NAME):$(VERSION)
DOCKER_LOCALHOST=$(shell /sbin/ifconfig docker0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $$1}')
CONFIG= -e "CON_CONNECTOR_INTERVAL=300" \
		-e "CON_CONNECTOR_REST_URL=https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/18/json" \
		-e "CON_CONNECTOR_REST_URL_KEY=M5DnCqGpE0Z1GtJv163YKYq1Y21W44Ub" \
		-e "CON_CONNECTOR_BASE_URL=https://demo-mobility.worldsensing.com/mb" \
		-e "CON_CONNECTOR_BASE_URL_USER=worldsensing" \
		-e "CON_CONNECTOR_BASE_URL_PASS=w0rlds3ns1ng2016" \
		
clean:
	@find . -iname "*~" | xargs rm 2>/dev/null || true
	@find . -iname "*.pyc" | xargs rm 2>/dev/null || true
	@find . -iname "build" | xargs rm -rf 2>/dev/null || true

minify: clean
	@mkdir -p build/minified && pyminifier --obfuscate --destdir=build/minified/ **/*.py

build: clean
	docker build -t $(DOCKER_NAME_FULL) .

build-minified: minify
	@cp Dockerfile requirements.txt build/
	docker build -t $(DOCKER_NAME_FULL) build/

run:
	@docker run -it --name $(CONNECTOR_CONTAINER_NAME) $(CONFIG) --rm $(DOCKER_NAME_FULL) $(CMD)

run-detached:
	@docker run -it --name $(CONNECTOR_CONTAINER_NAME) $(CONFIG) -d $(DOCKER_NAME_FULL) $(CMD)

publish: build
	@docker push $(DOCKER_NAME_FULL)

publish-minified: build-minified
	@docker push $(DOCKER_NAME_FULL)

tag_confirmation:
	@read -p "Are you sure to continue to create a tag (y/n)?" choice;\
		case "$$choice" in\
		y|Y ) echo "yes";;\
		n|N ) exit 1;;\
		* ) exit 1;;\
		esac

tag: tag_confirmation publish
	vim CHANGELOG.md
	@git commit -a
	@git push
	@git tag $(CONNECTOR_CONTAINER_NAME)_$(VERSION)
	@git push origin --tags