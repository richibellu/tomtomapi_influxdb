**Requirements**

- Ubuntu 16.04
- influxdb version 1.2.4
- influxdb-python version 4.1.1

**Installation tips**

To install influxdb from bash:

`wget https://dl.influxdata.com/influxdb/releases/influxdb_1.2.4_amd64.deb`

`sudo dpkg -i influxdb_1.2.4_amd64.deb`
If the installation unsucceds you may look for this:
https://groups.google.com/forum/#!topic/influxdb/UtaMjoFtotU

To install influxdb-python from bash:

`pip install influxdb`

**Usage**

Once downloaded, go to connector folder and run 

`python main_routine.py`

Data extraction can be tuned and changed looking at /assets/config.

Influxdb interaction can be tuned looking at tomtom.py and tomtom2influxdb.py

For more informations, look at the report:

https://docs.google.com/a/worldsensing.com/document/d/1F5i5mrIjxvVGja1faoZj3fvkRTi8vx2lB5Q6CMWgaJc/edit?usp=sharing