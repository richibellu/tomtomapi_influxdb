# Changelog

## 1.0.1 (2017-10-04)

- Added name information into COS
- Improved performance by reusing HTTP connections

## 1.0.0 (2017-10-04)

- Initial version