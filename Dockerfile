FROM python:2.7.12-alpine

ENV CONNECTOR_DIR /opt/mb/
ENV SOURCE_DIR connector

COPY requirements.txt ./

RUN apk add --no-cache --virtual .build-deps \
    build-base \
    && pip install -r requirements.txt \
    && find /usr/local \
    \( -type d -a -name test -o -name tests \) \
    -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
    -exec rm -rf '{}' + \
    && runDeps="$( \
    scanelf --needed --nobanner --recursive /usr/local \
    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
    | sort -u \
    | xargs -r apk info --installed \
    | sort -u \
    )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del .build-deps

RUN mkdir -p $CONNECTOR_DIR
WORKDIR $CONNECTOR_DIR
COPY $SOURCE_DIR $CONNECTOR_DIR/$SOURCE_DIR
WORKDIR $CONNECTOR_DIR/$SOURCE_DIR
CMD [ "python", "connector_main.py" ]
